<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
	<?php
	
		$testBoolean = true;
	
		include 'FormValidation.php';	//access the class file
	
		$validateTool = new FormValidation();	//instantiate a new object
	
		echo "Empty quotes: " . $validateTool->validateRequiredField("") . "<br>";
		echo "Single Space: " . $validateTool->validateRequiredField(" ") . "<br>";
		echo "Two Spaces: " . $validateTool->validateRequiredField("           ") . "<br>";	

		echo "a: " . $validateTool->validateRequiredField("a") . "<br>";
		echo "4: " . $validateTool->validateRequiredField(4) . "<br>";	
		echo "null: " . $validateTool->validateRequiredField(null) . "<br>";
		echo "NULL: " . $validateTool->validateRequiredField(NULL) . "<br>";
		echo "null string: " . $validateTool->validateRequiredField("null") . "<br>";	
		echo "undefined: " . $validateTool->validateRequiredField("undefined") . "<br>";
		echo "0: " . $validateTool->validateRequiredField(0) . "<br>";
		echo "?: " . $validateTool->validateRequiredField("?") . "<br>";	
	
	
	
		if($validateTool->validateRequiredField("a")) {
			echo "a is valid <br>";
		}else {
			echo "a is invalid <br>";
		}
	
		if($validateTool->validateRequiredField("")) {
			echo "empty is valid <br>";
		}else {
			echo "empty is invalid <br>";
		}
	
	?>
	<h3>Testing requiredNumericField</h3>
	<?php
	
		echo '7: ' . $validateTool->requiredNumericField(7) . "<br>";
		echo '0: ' . $validateTool->requiredNumericField(0) . "<br>";
		echo 'i: ' . $validateTool->requiredNumericField("i") . "<br>";	
		echo 'empty: ' . $validateTool->requiredNumericField("") . "<br>";		
		echo 'null: ' . $validateTool->requiredNumericField(null) . "<br>";		
		echo 'four: ' . $validateTool->requiredNumericField("four") . "<br>";	
		echo '.7: ' . $validateTool->requiredNumericField(.7) . "<br>";	
		echo '2.7: ' . $validateTool->requiredNumericField(2.7) . "<br>";		
		echo '-3: ' . $validateTool->requiredNumericField(-3) . "<br>";		
		echo '-3.3: ' . $validateTool->requiredNumericField(-3.3) . "<br>";	
		echo '+4: ' . $validateTool->requiredNumericField(+4) . "<br>";	
		echo '.: ' . $validateTool->requiredNumericField(".") . "<br>";		
		echo '$: ' . $validateTool->requiredNumericField("$") . "<br>";		
		echo '-: ' . $validateTool->requiredNumericField("-") . "<br>";		
		echo ',: ' . $validateTool->requiredNumericField(",") . "<br>";
		echo '2,300: ' . $validateTool->requiredNumericField("2,300") . "<br>";
		//$testNum = 2,300;
 
		echo '1 3: ' . $validateTool->requiredNumericField("1 3") . "<br>";		
		echo '7%: ' . $validateTool->requiredNumericField("7%") . "<br>";		
	
	?>
</body>
</html>





