DOM = Document Object Model.
OOP = Object Oriented Programming.
Object = Something you can use "a thing".
Property = Describe something about the object.
Methods = Activity the object can perform.
Class = Definition of what an object will be, defines the properties and methods of an object.
Constructor Function: Special method of a class or stucture in object-oriented programming that
		      initializes an object of that type.

PHP mail() Function
To(Required)
Subject(Required)
Message(Required)
Additional Headers(Optional)

Model = Data, Database.
View = HTML, CSS Display/Output.
Controller = Business Logic, "code goes here!". PHP code at top of page.

Methods:
Get = Publicly sends information, great for shopping websites.
Post = Privately sends information, should be used in almost all cases.

Setter: Sets value
Getter: Gets value

include 'emailer.php'; //get the class file
$customerMail = new emailer(); //instantiate a new object from Emailer class
