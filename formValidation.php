<?php

class FormValidation {
    
    public function validateRequiredField($inputValue) {
        if($inputValue == '') {
            return false;
        }
        else {
            return true;
        }
    }
    
    public function validateRequiredNumber($inputValue) {
       return is_numeric($inputValue);
    }
    
    public function validateSpecialCharacter($inputValue) {
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $inputValue))
        {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function validatePhoneNumber($inputValue) {
        if(mb_strlen($inputValue) == 10) {
            return true;
        }
        else {
            return false;
        };
    }
    
    public function validateEmail($inputValue) {
    	if(filter_var($inputValue, FILTER_VALIDATE_EMAIL) != false) {
      		return true;
    	}
    	else {
      		return false;
		}
  }
	
	public function validateLength($inputValue) {
		if(strlen($inputValue) > 200) {
			return false;
		}
		else {
			return true;
		}
	}
}

?>