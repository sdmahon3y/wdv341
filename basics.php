<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PHP Basics</title>

</head>
<body>

<?php

$yourName = "Sean Mahoney";

$number1 = 4;
$number2 = 2;
$total = $number1 + $number2;

$values = array("PHP", "HTML", "Javascript");


echo "<h1>PHP Basics</h1>";

?>

<h2>
<?php
echo $yourName;

echo "<p>First Number: $number1</p>";
echo "<p>Second Number: $number2</p>";
echo "<p>Total: $total</p>";
?>

<p>Please select a format: 
    <select name="select" id="select">
    	<option>Please select a format</option>
    </select>

<script>

var values = <?php echo '["' . implode('", "', $values) . '"]' ?>;
var valuesLength = values.length;

select = document.getElementById("select");

	for(var x = 0; x < valuesLength; x++) //For loop
	{
    	var y = values[x]; //Creates variable for current state
    	var opt = document.createElement("option"); //Creats a new option element
    	opt.textContent = y; //Content of option
    	opt.value = y; //Value of option
    	select.appendChild(opt); //Adds the option to dropdown menu
	} //I don't know if it's fine that I used this method, but I saw it and wanted to try it out.
	  //Can re-submit later if it needs changing

</script>
</p>

</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
 	function gtag(){dataLayer.push(arguments);}
 	gtag('js', new Date());

 	gtag('config', 'UA-146891297-1');
	</script>
</html>