<?php

class FormValidation {
    
    public function validateRequiredField($inputValue) {
        if($inputValue == '') {
            return false;
        }
        else {
            return true;
        }
    }
    
    public function validateRequiredNumber($inputValue) {
       return is_numeric($inputValue);
    }
    
    public function validateSpecialCharacter($inputValue) {
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $inputValue))
        {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function validatePhoneNumber($inputValue) {
        if(mb_strlen($inputValue) == 10) {
            return true;
        }
        else {
            return false;
        };
    }
    
    public function validateEmail($inputValue) {
    	if(filter_var($inputValue, FILTER_VALIDATE_EMAIL) != false) {
      		return true;
    	}
    	else {
      		return false;
		}
  }
	
	public function validateLength($inputValue) {
		if(strlen($inputValue) > 200) {
			return false;
		}
		else {
			return true;
		}
	}
    
    public function validateDate($inputValue) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $inputValue)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function validateTime($inputValue) {
        if (preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $inputValue)) {
            return true;
        } else {
            return false;
        }
    }
}

?>