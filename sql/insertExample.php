<?php
	//This xample uses SQL Prepared Statements with placeholders to
	//indicate where variable values can be placed in the SQL command

	require_once("dbConnector.php");

try {
		//SQL command using placeholders
	$sql = "INSERT INTO events (events_id, events_name, events_description)
		VALUES (NULL, :eName, :eDescription );";

	echo $sql;

	$statement = $conn->prepare($sql);

	$statement->execute();
}
catch(PDOException $e){
echo "Process failed: " . $e->getMessage();
}

$statement->bindParam(':eName', $eventName);
$statement->bindParam(':eDescription', $eventDescription);

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 SQL Insert</title>
</head>
<body>

    
    
</body>