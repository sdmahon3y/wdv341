<?php 
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
  
	if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
	{
		//User is already signed on.  Skip the rest.
		$message = "Welcome Back! $username";	//Create greeting for VIEW area		
	}

	else
	{
		if (isset($_POST['submitLogin']) )			//Was this page called from a submitted form?
		{
			$inUsername = $_POST['loginUsername'];	//pull the username from the form
			$inPassword = $_POST['loginPassword'];	//pull the password from the form
			
            try {
                
			include 'dbConnector.php';				//Connect to the database

			$sql = "SELECT user_id, event_username, event_password FROM event_user WHERE user_id = 1";				
			
			$stmt = $conn->prepare($sql);
			
			$stmt->execute();
            
            $users = $stmt->fetchAll();
            
            foreach ($users as $row) {
                
                if($row['event_username'] == $inUsername && $row['event_password'] == $inPassword) {
                    $_SESSION['validUser'] = "yes";
                    $message = "Welcome Back! $username";
                    
                    break;
                }
                
                else {
                    $_SESSION['validUser'] = "no";					
				    $message = "Sorry, there was a problem with your username or password. Please try again.";
                    }
                }
            }
            catch(PDOException $e){
                echo "Process failed: " . $e->getMessage();
                }//end if submitted
        }
        
		else {
			$message = ""; //user needs to see form
		}//end else submitted
        //end else valid user
}
//turn off PHP and turn on HTML
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Events Login</title>
</head>

<body>
<h1>WDV341 Intro PHP</h1>

<h2><?php echo $message?></h2>

<?php
	if ($_SESSION['validUser'] == "yes")	//This is a valid user.  Show them the Administrator Page
	{
		
//turn off PHP and turn on HTML
?>
		<h3>Events Administrator Options</h3>
        <p><a href="eventsForm.php">Input New Event</a></p>
        <p><a href="selectEventsAdmin.php">List of Events</a></p>
        <p><a href="logout.php">Logout of Admin System</a></p>	
        					
<?php
	}
	else									//The user needs to log in.  Display the Login Form
	{
?>
			<h2>Please login to the Administrator System</h2>
                <form method="post" name="loginForm" action="login.php" >
                  <p>Username: <input name="loginUsername" type="text" /></p>
                  <p>Password: <input name="loginPassword" type="password" /></p>
                  <p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
                </form>
                
<?php //turn off HTML and turn on PHP
	}//end of checking for a valid user
			
//turn off PHP and begin HTML			
?>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>