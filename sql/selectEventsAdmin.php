<?php

session_start();
 
if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
{
        
	require_once('dbConnector.php');

try {
    //create the sql command
	$sql = "SELECT event_id, event_name, event_description, event_presenter, DATE_FORMAT(event_date, '%c/%e/%Y') as event_formatted_date, LOWER(TIME_FORMAT(event_time, '%l:%i%p')) as event_formatted_time FROM wdv341_event ORDER BY event_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$events = $stmt->fetchAll();	//turn result set into an array.
    }

catch(PDOException $e){
    echo "Process failed: " . $e->getMessage();
    }
}

else {
    header("Location: http://sdmahoney.com/wdv341_finished/sql/selectEvents.php");
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SQL Admin Select</title>
</head>
<body>
<h1>SQL Admin Select</h1>
<?php
		//process each row of the array, displaying the event_name and event_presenter
		foreach ($events as $row) {
			echo "<div class='item'><div><a href='updateEventsForm.php?recId=" . $row['event_id'] . "'><input type='button' name='update' value='Update'></a><a onClick=\"javascript: return confirm('Are you sure you want to delete this event?');\" href='deleteEvent.php?recId=" . $row['event_id'] . "'><input type='button' name='delete' value='Delete'></a></div></div><br><div>" . $row['event_name'] . "</div><div>Date: " . $row['event_formatted_date'] . "</div><div>Time: " . $row['event_formatted_time']  . "</div><div>Event Description: " . $row['event_description'] . "</div><div> Presented by: " . $row['event_presenter'] .  "</div></div><br>";
		}
	?>

    
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>