<?php
	//This xample uses SQL Prepared Statements with placeholders to
	//indicate where variable values can be placed in the SQL command

	require_once("dbConnector.php");

try {
			//define the variables for the row
		$eventName = "WDV341";
		$eventDescription = "Introduces server side coding.";
		
		//SQL command using placeholders
	$sql = "INSERT INTO events (events_id, events_name, events_description)
		VALUES (NULL, :eName, :eDescription );";

	echo $sql;
		//Prepare your SQL Statement object
	$statement = $conn->prepare($sql);
	
		//Bind paremeters to the placeholders of the Statement object
	$statement->bindParam(':eName', $eventName);
	$statement->bindParam(':eDescription', $eventDescription);
	
		//Execute the Statement object with the associated values
	$statement->execute();
}
catch(PDOException $e){
echo "Process failed: " . $e->getMessage();
}

?>