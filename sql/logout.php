<?php
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
 
	if ($_SESSION['validUser'] == "yes") {
        
        $_SESSION['validUser'] = "no";
        header("Location: http://sdmahoney.com/wdv341_finished/sql/selectEvents.php");
        
    }

    else {
        header("Location: http://sdmahoney.com/wdv341_finished/sql/selectEvents.php");
    }

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Store Logout</title>
</head>

<body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</body>
</html>