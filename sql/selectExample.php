<?php

	require_once('dbConnector.php');

	//create the sql command
	$sql = "SELECT event_name, event_presenter, DATE_FORMAT(event_date, '%c/%e/%Y') as event_formatted_date, LOWER(TIME_FORMAT(event_time, '%l:%i%p')) as event_formatted_time FROM wdv341_event ORDER BY event_name";
	//prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$events = $stmt->fetchAll();	//turn result set into an array.
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Select Example</title>
</head>
<body>
	<h1>Event Catalog</h1>
	<h2>Listing of Current Events</h2>
	<?php
		//process each row of the array, displaying the event_name and event_presenter
		foreach ($events as $row) {
			echo "<div><div class='flex-container'><div>" . $row['event_name'] . "</div><div>Date: " . $row['event_formatted_date'] . "</div><div>Time: " . $row['event_formatted_time']  . "</div></div><div>Event Description: " . "</div><div> Presented by: " . $row['event_presenter'] .  "</div></div>";
		}
	?>
</body>
</html>