<?php
session_start();
 
if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
{
        
require_once('dbConnector.php');
    
include 'FormValidation.php';	//access the class file
	
$validateTool = new FormValidation();	//instantiate a new object

$inName = "";
$inDescription = "";
$inPresenter = "";
$inDate = "";
$inTime = "";

$errorName = "";
$errorDescription = "";
$errorPresenter = "";
$errorDate = "";
$errorTime = "";
$checkForm = "";

$updateRecID = $_GET['recId'];

//echo $updateRecID;

if($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(empty($_POST["testForm"])) {
        
        $inName = $_POST['name'];
        $inDescription = $_POST['description'];
        $inPresenter = $_POST['presenter'];
        $inDate = $_POST['date'];
        $inTime = $_POST['time'];
        
        if($validateTool->validateRequiredField($inName) == false) {
            $errorName = "Please enter event name.";
        }
        else {
            if($validateTool->validateSpecialCharacter($inName) == true) {
                $errorName = "Please remove any special characters.";
            }
            else {
                if($validateTool->validateLength($inName) == false) {
                $errorName = "Please use less than 200 characters.";
                }
                else {
                $errorName = "";
                }
            }
        }
        
        if($validateTool->validateRequiredField($inDescription) == false) {
            $errorDescription = "Please enter description.";
        }
        else {
            if($validateTool->validateLength($inDescription) == false) {
                $errorDescription = "Please use less than 200 characters.";
            }
            else {
                $errorDescription = "";
            }
        }
        
        if($validateTool->validateRequiredField($inPresenter) == false) {
            $errorPresenter = "Please enter presenter.";
        }
        else {
            if($validateTool->validateLength($inPresenter) == false) {
                $errorPresenter = "Please use less than 200 characters.";
            }
            else {
                $errorPresenter = "";
            }
        }
        
        if($validateTool->validateRequiredField($inDate) == true) {
            if($validateTool->validateDate($inDate) == true) {
                $errorDate = "";
            }
            else {
                $errorDate = "Please use date format on form and re-submit.";
            }
        }
        else {
            $errorDate = "Please enter year.";
        }
        
        if($validateTool->validateRequiredField($inTime) == true) {
            if($validateTool->validateTime($inTime) == true) {
                $errorTime = "";
            }
            else {
                $errorTime = "Please use time format on form and re-submit.";   

            }
        }
        else {
            $errorTime = "Please enter event time.";  
        }
           
        $checkForm = $errorName . $errorDescription . $errorPresenter . $errorDate . $errorTime;
        
        if($checkForm == "") {
        
            try {
                
            require_once("dbConnector.php");
            //SQL command using placeholders
	        
            $sql = "UPDATE wdv341_event SET ";
            $sql .= "event_name='$inName', ";
            $sql .= "event_description='$inDescription', ";
            $sql .= "event_presenter='$inPresenter', ";
            $sql .= "event_date='$inDate', ";
            $sql .= "event_time='$inTime' ";
            $sql .= "WHERE event_id='$updateRecID'";

	        //echo $sql;

	        $statement = $conn->prepare($sql);
                
	        $statement->execute();
                
            echo "Event Updated";
                
            }
            catch(PDOException $e){
                echo "Process failed: " . $e->getMessage();
                }

        }
        else {
            echo "<script>console.log('Please fix any errors in input fields before submitting again.');</script>";
        }
    }
}

else {
    try {
        
    require_once("dbConnector.php");
    //create the sql command
        
	$sql = "SELECT event_id, event_name, event_description, event_presenter, DATE_FORMAT(event_date, '%Y-%m-%d') as event_formatted_date, LOWER(TIME_FORMAT(event_time, '%T')) as event_formatted_time FROM wdv341_event WHERE event_id=$updateRecID";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
        
    $stmt->setFetchMode(PDO::FETCH_ASSOC);	
		  
    $row=$stmt->fetch(PDO::FETCH_ASSOC);	 
				
        $inName=$row['event_name'];
        $inDescription=$row['event_description'];
        $inPresenter=$row['event_presenter'];
        $inDate=$row['event_formatted_date'];
        $inTime=$row['event_formatted_time'];
        
}

catch(PDOException $e){
echo "Process failed: " . $e->getMessage();
}
}
}

else {
    header("Location: http://sdmahoney.com/wdv341_finished/sql/selectEvents.php");
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Events Form</title>
<style>

#orderArea	{
	width:600px;
	border:thin solid black;
	margin: auto auto;
	padding-left: 20px;
}

#orderArea h3	{
	text-align:center;	
}
.error	{
	color:red;
	font-style:italic;	
}
    
#testForm {
    display: none;
}
    
span {
	color: red;
}
</style>
</head>

<body>

<h3>Event Insert Form</h3>
<form id="updateEventForm" name="updateEventForm" method="post" action="updateEventsForm.php?recId=<?php echo $updateRecID ?>">
    <p>
        <input type="text" name="test" id="testForm" value="">
        <label for="name">Event Name:</label>
        <input type="text" name="name" id="name" value="<?php echo $inName ?>"> <span><?php echo $errorName ?></span>
    </p>
    <p>
        <label for="description">Event Description:</label>
        <input type="text" name="description" id="description" value="<?php echo $inDescription ?>"> <span><?php echo $errorDescription ?></span>
    </p>
    <p>
        <label for="presenter">Event Presenter: </label>
        <input type="text" name="presenter" id="presenter" value="<?php echo $inPresenter ?>"> <span><?php echo $errorPresenter ?></span>
    </p>
    <p>
        <label for="date">YYYY-MM-DD Event Date: </label>
        <input type="text" name="date" id="date" value="<?php echo $inDate ?>"> <span><?php echo $errorDate ?></span>
    </p>
    <p>
        <label for="time">HH:MM:SS Event Time: </label>
        <input type="text" name="time" id="time" value="<?php echo $inTime ?>"> <span><?php echo $errorTime ?></span>
    </p>
    <p>
    <input type="submit" name="button3" id="button3" value="Submit">
    <input type="button" name="button4" id="button4" value="Reset">
  </p>
    </form>
    
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>