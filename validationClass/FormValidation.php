<?php
/*
  This class will hold a variety of general purpose methods for validating forms.
  The methods will accept inputs as needed and will return true if the values pass the
  validation and false otherwise.
*/
# Author: brvdleyowens;
# Copyright: 2019;

class FormValidation {
  # Properties - Properties to be used within the object

  # Constructor - Initiallizes contents of new object

  public function __construct() {
    // Empty constructor with no defined process - added for completeness.
  }

  # Methods - Functions for validating

    //Required Field Validation
  public function validateRequiredField($inputValue) {

  if($inputValue == '' /* || !strcasecmp($inputValue, "null") || !strcasecmp($inputValue, "undefined") */) {
      return false;
    }
    else {
      return true;
    }
  }
  //Required Numeric Validation
  public function validateRequiredNumeric($inputValue) {
    return is_numeric($inputValue);
  }
  //Email Validation
  public function validateEmail($inputValue) {
    if (filter_var($inputValue, FILTER_VALIDATE_EMAIL) != false) {
      return true;
    }
    else {
      return false;
    }
  }

  public function validatePhone($inputValue) {
    if (filter_var($inputValue, FILTER_SANITIZE_NUMBER_INT) != false) {
      $code = rand(100,999);
      mail("6412038710@email.uscc.net", "", "Please Verify Phone Number: Your Code is $code", "From: Test Center <brvdley@brvdleyowens.com> \r\n");
      return true;
    }
    else {
      return false;
    }
  }



}
?>
