<?php

include 'FormValidation.php';	//access the class file
	
$validateTool = new FormValidation();	//instantiate a new object

$inName = "";
$inPhoneNumber = "";
$inEmailAddress = "";
$inRegister = "";
$inBadge = "";
$inFriday = "";
$inSaturday = "";
$inSunday = "";
$inRequest = "";

$errorName = "";
$errorPhoneNumber = "";
$errorEmailAddress = "";
$errorRegister = "";
$errorBadge = "";
$errorRequest = "";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(empty($_POST["testForm"])) {
	 
        $inName = $_POST["name"];
        $inPhoneNumber = $_POST["phoneNumber"];
        $inEmailAddress = $_POST["emailAddress"];
        $inRegister = $_POST["register"];
		$inRequest = $_POST["request"];
		
		if(!empty($_POST['fridayDinner'])) {
			$inFriday = $_POST["fridayDinner"];
			}
		if(!empty($_POST['saturdayLunch'])) {
			$inSaturday = $_POST["saturdayLunch"];
			}
		if(!empty($_POST['sundayBrunch'])) {
			$inSunday = $_POST["sundayBrunch"];
			}
        	
			if($validateTool->validateRequiredField($inName) == false) {
				$errorName = "Please enter your name";
				}
			
			if($validateTool->validateRequiredField($inPhoneNumber) == true) {
				if($validateTool->validateSpecialCharacter($inPhoneNumber) == false) {
					if($validateTool->validateRequiredNumber($inPhoneNumber) == true) {
						if($validateTool->validatePhoneNumber($inPhoneNumber) == false) {
							$errorPhoneNumber = "Please enter ten-digit phone number.";
							}
						}
					else {
							$errorPhoneNumber = "Please enter ten-digit phone number.";
						}
					}
				else {
					$errorPhoneNumber = "Please enter ten-digit phone number.";
					}
				}
			else {
				$errorPhoneNumber = "Please enter your phone number.";
				}
			
			if($validateTool->validateRequiredField($inEmailAddress) == true) {
				if($validateTool->validateEmail($inEmailAddress) == true) {
					
					}
				else {
					$errorEmailAddress = "Please enter a valid email address";
					}
				}
			else {
				$errorEmailAddress = "Please enter your email address";
				}
				
			if($inRegister == "choose") {
                $errorRegister = "Please choose a registration option.";
        		}
		
			if($inBadge == "") {
                $errorBadge = "Please choose a badge.";
        		}
			else {
				$inBadge = $_POST["badge"];
				}
		
			if($validateTool->validateLength($inRequest) == true) {
				if($validateTool->validateSpecialCharacter($inRequest) == true) {
					$errorRequest = "Please remove any special characters.";
					}
				}
			else {
				$errorRequest = "Over 200 character limit.";
			}
		
        echo "<script>console.log('Form Submitted');</script>";
        }
    
    else {
        echo "Form failed due to spam";
        }
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Self Posting Form</title>
<style>

#orderArea	{
	width:600px;
	border:thin solid black;
	margin: auto auto;
	padding-left: 20px;
}

#orderArea h3	{
	text-align:center;	
}
.error	{
	color:red;
	font-style:italic;	
}
    
#testForm {
    display: none;
}

span {
	color: red;
}
	
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Unit-5 and Unit-6 Self Posting - Form Validation Assignment


</h2>
<p>&nbsp;</p>


<div id="orderArea">
<form name="form3" method="post" action="customerRegistrationForm.php">
  <h3>Customer Registration Form</h3>

      <p>
		<input type="text" name="test" id="testForm" value="">
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="<?php echo $inName ?>"> <span><?php echo $errorName ?></span>
      </p>
    
      <p>
        <label for="phoneNumber">Phone Number:</label>
        <input type="text" name="phoneNumber" id="phoneNumber" value="<?php echo $inPhoneNumber ?>"> <span><?php echo $errorPhoneNumber ?></span>
      </p>
      <p>
        <label for="emailAddress">Email Address: </label>
        <input type="text" name="emailAddress" id="emailAddress" value="<?php echo $inEmailAddress ?>"> <span><?php echo $errorEmailAddress ?></span>
      </p>
      <p>
        <label for="register">Registration: </label>
        <select name="register" id="select">
          <option value="choose">Choose Type</option>
          <option value="attendee" <?php if($inRegister == "attendee")
                {
                echo "selected";
                } ?>>Attendee</option>
          <option value="presenter" <?php if($inRegister == "presenter")
                {
                echo "selected";
                } ?>>Presenter</option>
          <option value="volunteer" <?php if($inRegister == "volunteer")
                {
                echo "selected";
                } ?>>Volunteer</option>
          <option value="guest" <?php if($inRegister == "guest")
                {
                echo "selected";
                } ?>>Guest</option>
        </select> <span><?php echo $errorRegister ?></span>
      </p>
      <p>Badge Holder: <span><?php echo $errorBadge ?></span></p>
      <p>
        <input type="radio" name="badge" id="clip" value="clip" <?php
            if($inBadge == "clip")
            {
                echo "checked";
            } ?>>
        <label for="badge">Clip</label> <br>
        <input type="radio" name="badge" id="lanyard" value="lanyard" <?php
            if($inBadge == "lanyard")
            {
                echo "checked";
            } ?>>
        <label for="badge">Lanyard</label> <br>
        <input type="radio" name="badge" id="magnet" value="magnet" <?php
            if($inBadge == "magnet")
            {
                echo "checked";
            } ?>>
        <label for="badge">Magnet</label>
      </p>
      <p>Provided Meals (Select all that apply):</p>
      <p>
        <input type="checkbox" name="fridayDinner" id="fridayDinner" value="friday" <?php
            if($inFriday == "friday")
            {
                echo "checked";
            } ?>>
        <label for="fridayDinner">Friday Dinner</label><br>
        <input type="checkbox" name="saturdayLunch" id="saturdayLunch" value="saturday" <?php 
            if($inSaturday == "saturday") 
            {
                echo "checked";
            } ?>>
        <label for="saturdayLunch">Saturday Lunch</label><br>
        <input type="checkbox" name="sundayBrunch" id="sundayBrunch" value="sunday" <?php 
            if($inSunday == "sunday") 
            {
                echo "checked";
            } ?>>
        <label for="sundayBrunch">Sunday Award Brunch</label>
      </p>
      <p>
        <label for="request">Special Requests/Requirements: (Limit 200 characters)<br>
        </label>
        <textarea name="request" cols="40" rows="5" id="request" value=""><?php echo $inRequest ?></textarea>
		  <span><?php echo $errorRequest ?></span>
      </p>
   
  <p>
    <input type="submit" name="button3" id="button3" value="Submit">
    <input type="button" name="button4" id="button4" value="Reset">
  </p>
</form>
</div>

</body>
</html>