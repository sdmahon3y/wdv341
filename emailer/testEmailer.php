<?php

	include 'emailer.php'; //get the class file

	$customerMail = new emailer(); //instantiate a new object from Emailer class and constructor initializes

	$customerMail->setRecipientAddress("sdmahoney@dmacc.edu");

	$customerMail->getRecipientAddress();

	$customerMail->setEmailMessage("Hello There");

	$customerMail->getEmailMessage();

	$customerMail->setEmailSubject("Greetings");

	$customerMail->getEmailSubject();

	$customerMail->setSenderAddress("contact@sdmahoney.com");

	$customerMail->getSenderAddress();

	$customerMail->sendEmail();	//sends the email

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Emailer Test</title>
	</head>

<body>
	<h1>WDV341 Intro PHP</h1>
	<h2>Test Emailer Class</h2>
	
	<p>RecipientAddress: <?php echo $customerMail->getRecipientAddress(); ?></p>
	<p>Subject: <?php echo $customerMail->getEmailSubject(); ?></p>
	<p>Message: <?php echo $customerMail->getEmailMessage(); ?></p>
	<p>From: <?php echo $customerMail->getSenderAddress(); ?></p>

</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>