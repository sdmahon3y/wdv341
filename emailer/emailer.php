<?php

class emailer {
	
	//The class will store information required to send a PHP email.
	//It will build an email and use the PHP mail().
	
	//Properties of the class
	
	private $emailMessage;		//body of the email
		
	private $emailSubject;
	
	private $recipientAddress;
	
	private $senderAddress;
		
	//Constructor_funtion
	
	function __construct() {
	
	}
	
	//Setting functions aka mutators
	
	function setEmailMessage($inMessage) {
		$this->emailMessage = $inMessage;
		}
	
	function setEmailSubject($inSubject) {
		$this->emailSubject = $inSubject;
	}
	
	function setRecipientAddress($inToAddress) {
		$this->recipientAddress = $inToAddress;
	}
	
	function setSenderAddress($inFromAddress) {
		$this->senderAddress = $inFromAddress;
	}
 
	//Getting function	aka accessors
	
	function getEmailMessage() {
		return $this->emailMessage;
	}
	
	function getEmailSubject() {
		return $this->emailSubject;
	}
	
	function getRecipientAddress() {
		return $this->recipientAddress;
	}
	
	function getSenderAddress() {
		return $this->senderAddress;
	}
	
	//Processing functions
	
	function sendEmail(){
		
		$fromAddress = "From: " . $this->getSenderAddress();
		
		mail($this->getRecipientAddress(),
			$this->getEmailSubject(),
			$this->getEmailMessage(),
			$fromAddress
			);
		echo "Your mail has been sent successfuly ! Thank you for your feedback";
	}
	
}

?>