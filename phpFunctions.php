<?php

setlocale(LC_MONETARY,"en_US");

function todaysDate()
{
    $mydate=getdate(date("U"));
    return date('n/j/Y');
}

function internationalDate()
{
    $mydate=getdate(date("U"));
    return date('Y/m/d');
}

function countString()
{
    $stringTest = " Sean Mahoney ";
    return strlen($stringTest);
}

function trimString()
{
    $stringTest = " Sean Mahoney ";
    return trim($stringTest,"");
}

function lowerString()
{
    $stringTest = " Sean Mahoney ";
    return strtolower($stringTest); 
}

function containsString()
{
    $stringTest = " Sean Mahoney ";
    
    if(stristr($stringTest, "DMACC")) {
        $result = "True";
    } 
    else {
        $result =  "False";
    }
       
    return $result;
}

function formatNumber()
{
    $numberTest1 = 1234567890;
    return number_format($numberTest1,2)."<br>";
}
       
function formatCurrency()
{
    $numberTest2 = 123456;
    return money_format("%n", $numberTest2);   
}
       
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PHP Functions</title>
</head>

<body>
<h1>PHP Functions</h1>
<h3>Date: <?php echo todaysDate(); ?></h3> 
<h3>International Date: <?php echo internationalDate(); ?></h3>
<h3>Input String: <input type="text" name="string" id="string"/></h3>
<p>Char Count: <?php echo countString(); ?></p>
<p>Trimmed: <?php echo trimString(); ?></p>
<p>Lower Case: <?php echo lowerString(); ?></p>
<p>Contains DMACC?: <?php echo containsString(); ?></p>
<h3>Formatted Number: <?php echo formatNumber(); ?></h3>
<h3>Formatted Currency: <?php echo formatCurrency(); ?></h3>
</body>
</html>