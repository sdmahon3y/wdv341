<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Code Segments</title>
</head>

<body>

	<h1>WDV341 - PHP Code Segments</h1>
    <h2>Example 1 - All output is written by PHP echo commands</h2>

	<?php
		
		$validInput = true;		//boolean variable
		
		if($validInput)
		{
			//true branch
			echo "<h2>Good Job! Thank You</h2>";
		}
		else
		{
			//else branch
			echo "<h2>Sorry Try Again!<h2>";
		}
	
	
	?>
    
    <h2>Example 2 - all output is passed to the Response Object as HTML line</h2>
    
    <?php
	
		$validTwoInput = false;		//boolean variable
		
		if($validTwoInput)
		{	
	?>
    	<h2>Good Job! Thank You</h2>
        <p>Your information ha been entered. You will be contacted when it is ready</p>
       	<p>For updates <a href="http://www.google.com">Current Status</a></p>
	<?php
		}
		else
		{
	?>
    	<h2>Sorry Try Again</h2>
        <p>Your form has encountered several errors.</p>
        <p>Please fix all of the following errors. We cannot use your information until all errors are resolved.</p>
        <p>For assistance please contact <a href="http://www.google.com">Google</a> during normal business hours.</p>
        
	<?php
		}
	?>
    
</body>
</html>