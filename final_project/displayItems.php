<?php

	require_once('dbConnector.php');

try {
    //create the sql command
	$sql = "SELECT item_id, item_name, item_description, item_cost, item_image FROM item ORDER BY item_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$events = $stmt->fetchAll();	//turn result set into an array.
}

catch(PDOException $e){
echo "Process failed: " . $e->getMessage();
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SQL Select</title>
<link href ="css/final_project.css" rel = "stylesheet" type = "text/css" />
</head>
<body>
<div id = "container">
<header><section id="top"></section>
<h1>Welcome to Sean's Furniture Store</h1>
<nav>
    <ul>
        <li><a href="displayItems.php">Shop</a></li>
        <li><a href="index.php">About Us</a></li>
        <li><a href="contactForm.php">Contact Us</a></li>
        <li><a href="storeLogin.php">Login</a></li>
    </ul>
</nav>
</header>
<div class="flex-container">
<?php
		//process each row of the array, displaying the event_name and event_presenter
		foreach ($events as $row) {
			echo "<section><div>Item Name: " . $row['item_name'] . "</div><br><div>Description: " . $row['item_description'] . "</div><br><div>Cost: $" . $row['item_cost']  . "</div><br><div><img src='images/" . $row['item_image'] . "' class='item_img'></div><br></section>";
		}
	?>
</div>
</div>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>