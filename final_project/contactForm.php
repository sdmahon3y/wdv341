<?php

include 'FormValidation.php';	//access the class file
	
$validateTool = new FormValidation();	//instantiate a new object

$inFirstName = "";
$inLastName = "";
$inEmail = "";
$inMessage = "";
$checkForm = "";

$inFirstNameError = "";
$inLastNameError = "";
$inEmailError = "";
$inMessageError = "";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(empty($_POST["testForm"])) {
		
	if($validateTool->validateRequiredField($inFirstName) == false) {
            $inFirstNameError = "Please enter item name.";
        }
        else {
            if($validateTool->validateSpecialCharacter($inFirstName) == true) {
                $inFirstNameError = "Please remove any special characters.";
            }
            else {
                if($validateTool->validateLength($inFirstName) == false) {
                $inFirstNameError = "Please use less than 200 characters.";
                }
                else {
                $inFirstNameError = "";
                }
            }
        }
		
	if($validateTool->validateRequiredField($inLastName) == false) {
            $inLastNameError = "Please enter item name.";
        }
        else {
            if($validateTool->validateSpecialCharacter($inLastName) == true) {
                $inLastNameError = "Please remove any special characters.";
            }
            else {
                if($validateTool->validateLength($inLastName) == false) {
                $inLastNameError = "Please use less than 200 characters.";
                }
                else {
                $inLastNameError = "";
                }
            }
        }
	
	if($validateTool->validateRequiredField($inEmail) == true) {
				if($validateTool->validateEmail($inEmail) == true) {
					$inEmailError = "";
					}
				else {
					$inEmailError = "Please enter a valid email address";
					}
				}
		else {
			$inEmailError = "Please enter your email address";
			}
	
	if($validateTool->validateRequiredField($inMessage) == false) {
		$inMessageError = "Please enter message.";
	}
	else {
		if($validateTool->validateLength($inMessage) == false) {
			$inMessageError = "Please use less than 200 characters.";
		}
		else {
			$inMessageError = "";
		}
	}
	
	$checkForm = $inFirstNameError . $inLastNameError . $inEmailError . $inMessageError;
		
		if($checkForm == "") {
		
			
			
			
		}
	
	}
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Contact Form</title>
<link href ="css/final_project.css" rel = "stylesheet" type = "text/css" />
<style>

#orderArea	{
	width:600px;
	border:thin solid black;
	margin: auto auto;
	padding-left: 20px;
}

#orderArea h3	{
	text-align:center;	
}
.error	{
	color:red;
	font-style:italic;	
}
    
#testForm {
    display: none;
}

span {
	color: red;
}
</style>
</head>

<body>
<div id = "container">
<header><section id="top"></section>
<h1>Welcome to Sean's Furniture Store</h1>
<nav>
    <ul>
        <li><a href="displayItems.php">Shop</a></li>
        <li><a href="index.php">About Us</a></li>
		<li><a href="contactForm.php">Contact Us</a></li>
        <li><a href="storeLogin.php">Login</a></li>
    </ul>
</nav>
</header>
<h2>Contact Us</h2>
<form name="contact" method="post" action="contactForm.php">
	<legend>Send us a Message</legend>
		<input type="text" name="testForm" id="testForm">
	<p>
		<label for="firstname" id = "firstname">First Name</label>
		<input type="text" name="firstname" id="firstname" size="40" value="<?php echo $inFirstName ?>"/> <?php echo $inFirstNameError ?>
	</p>
	<p>
		<label for="lastname">Last Name</label>
		<input type="text" name="lastname" id="lastname" size="40" value="<?php echo $inLastName ?>"/> <?php echo $inLastNameError ?>
	</p>
	<p>	
		<label for="email">Email Address</label>
		<input type="email" name="email" id="email" size="40" value="<?php echo $inEmailError ?>"/> <?php echo $inEmailError ?>
	</p>
	<p>
		<label for "message">Message</label>
		<textarea name= "message" id = "message" rows = "10" cols = "38" value="<?php echo $inMessage ?>"></textarea> <?php echo $inMessageError ?>
	</p>
	<p>
		<input type = "submit" value = "Send" class = "button" />
		<input type = "reset" value= "Reset" class = "button" />
	</p>
</form>
</div>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>