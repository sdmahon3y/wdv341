<?php
session_start();

$_SESSION['validUser'] = "no";

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Home Page</title>
<link href ="css/final_project.css" rel = "stylesheet" type = "text/css" />
    
</head>

<body>
<div id = "container">
<header><section id="top"></section>
<h1>Welcome to Sean's Furniture Store</h1>
<nav>
    <ul>
        <li><a href="displayItems.php">Shop</a></li>
        <li><a href="index.php">About Us</a></li>
        <li><a href="contactForm.php">Contact Us</a></li>
        <li><a href="storeLogin.php">Login</a></li>
    </ul>
</nav>
</header>
<img src="images/home_img.jpg" alt="corner sofa" class="home_img">
<h3>About Us</h3>
<p>With over thirty years in business, we have all your furniture needs from dining room to living room furniture and even outdoor furniture. Check out our great deals on buying in bulk as well as our seasonal sales. We also offer an assortment of coupons, enter your coupon code in your cart page for usage. We ship for free within the United States and with a small flat rate within Canada. Taxes will be calculated at checkout.</p>
</div>

</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>