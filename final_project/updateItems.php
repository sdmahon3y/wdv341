<?php

session_start();
 
	if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
	{
        
        include 'FormValidation.php';	//access the class file
	
        $validateTool = new FormValidation();	//instantiate a new object

        $inName = "";
        $inDescription = "";
        $inCost = "";
        $inImage = "";

        $errorName = "";
        $errorDescription = "";
        $errorCost = "";
        $errorImage = "";
        $checkForm = "";

        $updateRecID = $_GET['recId'];

        //echo $updateRecID;

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
       
            if(empty($_POST["testForm"])) {
        
            $inName = $_POST['name'];
            $inDescription = $_POST['description'];
            $inCost = $_POST['cost'];
            $inImage = $_POST['image'];
        
            if($validateTool->validateRequiredField($inName) == false) {
                $errorName = "Please enter item name.";
            }
            else {
                if($validateTool->validateSpecialCharacter($inName) == true) {
                    $errorName = "Please remove any special characters.";
                }
                else {
                    if($validateTool->validateLength($inName) == false) {
                        $errorName = "Please use less than 200 characters.";
                    }
                    else {
                    $errorName = "";
                    }
                }
            }
        
            if($validateTool->validateRequiredField($inDescription) == false) {
                $errorDescription = "Please enter description.";
            }
            else {
                if($validateTool->validateLength($inDescription) == false) {
                    $errorDescription = "Please use less than 200 characters.";
                }   
                else {
                    $errorDescription = "";
                }
            }
        
            if($validateTool->validateRequiredField($inCost) == false) {
                $errorCost = "Please enter presenter.";
            }
            else {
                if($validateTool->validateRequiredNumber($inCost) == false) {
                    $errorCost = "Please use less than 200 characters.";
                }
                else {
                    $errorCost = "";
                }
            }
        
            if($validateTool->validateRequiredField($inImage) == false) {
                $errorImage = "Please enter file name.";
                }
            else {
                $errorImage = "";
            }
           
            $checkForm = $errorName . $errorDescription . $errorCost . $errorImage;
        
            if($checkForm == "") {
            
                try {
                
                require_once("dbConnector.php");
                //SQL command using placeholders
	        
                $sql = "UPDATE item SET ";
                $sql .= "item_name='$inName', ";
                $sql .= "item_description='$inDescription', ";
                $sql .= "item_cost='$inCost', ";
                $sql .= "item_image='$inImage' ";
                $sql .= "WHERE item_id='$updateRecID'";

	           //echo $sql;

	           $statement = $conn->prepare($sql);
                
	           $statement->execute();
            
                header("Location: http://sdmahoney.com/wdv341_finished/final_project/displayItemsAdmin.php");
                echo "<script>alert('Item Updated')</script>";
                
                }
            
                catch(PDOException $e){
                    echo "Process failed: " . $e->getMessage();
                    }

            }
            else {
                echo "<script>console.log('Please fix any errors in input fields before submitting again.');</script>";
            }
        }
    }

    else {
        try {
        
        require_once("dbConnector.php");
        //create the sql command
        
	    $sql = "SELECT item_id, item_name, item_description, item_cost, item_image FROM item WHERE item_id=$updateRecID";

        //prepare the sql statement
	   $stmt = $conn->prepare($sql);
	   //bind the parameters if any
	   //execute the statement
	   $stmt->execute();
	   //Work with the result-set from the SELECT command
        
        $stmt->setFetchMode(PDO::FETCH_ASSOC);	
		  
        $row=$stmt->fetch(PDO::FETCH_ASSOC);	 
				
            $inName=$row['item_name'];
            $inDescription=$row['item_description'];
            $inCost=$row['item_cost'];
            $inImage=$row['item_image'];
        
        }

        catch(PDOException $e){
            echo "Process failed: " . $e->getMessage();
        }
    }
}

else {
    header("Location: http://sdmahoney.com/wdv341_finished/final_project/index.php");
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Items</title>
<link href ="css/final_project.css" rel = "stylesheet" type = "text/css" />
<style>

#orderArea	{
	width:600px;
	border:thin solid black;
	margin: auto auto;
	padding-left: 20px;
}

#orderArea h3	{
	text-align:center;	
}
.error	{
	color:red;
	font-style:italic;	
}
    
#testForm {
    display: none;
}

span {
	color: red;
}
</style>
</head>

<body>
<div id = "container">
<header><section id="top"></section>
<h1>Welcome to Sean's Furniture Store</h1>
<nav>
    <ul>
        <li><a href="displayItemsAdmin.php">Display Items</a></li>
        <li><a href="insertItems.php">Insert Item</a></li>
        <li><a href="storeLogout.php">Logout</a></li>
    </ul>
</nav>
</header>
<h3>Item Update Form</h3>
<form name="insert" method="post" action="updateItems.php?recId=<?php echo $updateRecID ?>">
    <p>
        <input type="text" name="test" id="testForm" value="">
        <label for="name">Item Name:</label>
        <input type="text" name="name" id="name" value="<?php echo $inName ?>"> <span><?php echo $errorName ?></span>
    </p>
    <p>
        <label for="description">Item Description:</label>
        <input type="text" name="description" id="description" value="<?php echo $inDescription ?>"> <span><?php echo $errorDescription ?></span>
    </p>
    <p>
        <label for="cost">Item Cost: </label>
        <input type="text" name="cost" id="cost" value="<?php echo $inCost ?>"> <span><?php echo $errorCost ?></span>
    </p>
    <p>
        <label for="image">Item Image Name: </label>
        <input type="text" name="image" id="image" value="<?php echo $inImage ?>"> <span><?php echo $errorImage ?></span>
    </p>
    <p>
    <input type="submit" name="button3" id="button3" value="Submit">
    <input type="button" name="button4" id="button4" value="Reset">
  </p>
    </form>
</div>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>