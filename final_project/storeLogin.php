<?php 
session_cache_limiter('none');			//This prevents a Chrome error when using the back button to return to this page.
session_start();
 
	if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
	{
		//User is already signed on.  Skip the rest.
		$message = "Welcome Back!";	//Create greeting for VIEW area		
	}
	else
	{
		if (isset($_POST['submitLogin']) )			//Was this page called from a submitted form?
		{
			$inUsername = $_POST['loginUsername'];	//pull the username from the form
			$inPassword = $_POST['loginPassword'];	//pull the password from the form
			
            try {
                
			include 'dbConnector.php';				//Connect to the database

			$sql = "SELECT store_id, store_username, store_password FROM store_user WHERE store_id = 1";				
			
			$stmt = $conn->prepare($sql);
			
			$stmt->execute();
            
            $users = $stmt->fetchAll();
            
            foreach ($users as $row) {
                
                if($row['store_username'] == $inUsername && $row['store_password'] == $inPassword) {
                    $_SESSION['validUser'] = "yes";
                    $message = "Welcome Back! $username";
                    
                }
                
                else {
                    $_SESSION['validUser'] = "no";					
				    $message = "Sorry, there was a problem with your username or password. Please try again.";
                    }
                }
            }
            catch(PDOException $e){
                echo "Process failed: " . $e->getMessage();
                }//end if submitted
        }
        
		else {
			$message = ""; //user needs to see form
		}//end else submitted
        //end else valid user
}
//turn off PHP and turn on HTML
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Store Login</title>
</head>

<body>
<h2><?php echo $message?></h2>

<?php
	if ($_SESSION['validUser'] == "yes")	//This is a valid user.  Show them the Administrator Page
	{
		
//turn off PHP and turn on HTML
?>
		<h3>Store Administrator Options</h3>
        <p><a href="insertItems.php">Input New Item</a></p>
        <p><a href="displayItemsAdmin.php">List of Items</a></p>
        <p><a href="storeLogout.php">Logout of Admin System</a></p>	
        					
<?php
	}
	else									//The user needs to log in.  Display the Login Form
	{
?>
			<h2>Please login to the Administrator System</h2>
                <form method="post" name="loginForm" action="storeLogin.php" >
                  <p>Username: <input name="loginUsername" type="text" /></p>
                  <p>Password: <input name="loginPassword" type="password" /></p>
                  <p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
                </form>
                
<?php //turn off HTML and turn on PHP
	}//end of checking for a valid user
			
//turn off PHP and begin HTML			
?>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>