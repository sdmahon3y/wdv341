<?php

session_start();
 
	if ($_SESSION['validUser'] == "yes")				//is this already a valid user?
	{
        
	require_once('dbConnector.php');

        try {
        //create the sql command
	    $sql = "SELECT item_id, item_name, item_description, item_cost, item_image FROM item ORDER BY item_name";

        //prepare the sql statement
	   $stmt = $conn->prepare($sql);
	   //bind the parameters if any
	   //execute the statement
	   $stmt->execute();
	   //Work with the result-set from the SELECT command
	   $events = $stmt->fetchAll();	//turn result set into an array.
        }

        catch(PDOException $e){
            echo "Process failed: " . $e->getMessage();
        }
    }

    else {
        header("Location: http://sdmahoney.com/wdv341_finished/final_project/index.php");  
    }

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SQL Select</title>
<link href ="css/final_project.css" rel = "stylesheet" type = "text/css" />
</head>
<body>
<div id = "container">
<header><section id="top"></section>
<h1>Welcome to Sean's Furniture Store</h1>
<nav>
    <ul>
        <li><a href="displayItemsAdmin.php">Display Items</a></li>
        <li><a href="insertItems.php">Insert Item</a></li>
        <li><a href="storeLogout.php">Logout</a></li>
    </ul>
</nav>
</header>
<div class="flex-container">
<?php
		//process each row of the array, displaying the event_name and event_presenter
		foreach ($events as $row) {
			echo "<section><div><a href='updateItems.php?recId=" . $row['item_id'] . "'><input type='button' name='update' value='Update' class='button'></a><a onClick=\"javascript: return confirm('Are you sure you want to delete this item?');\" href='deleteItems.php?recId=" . $row['item_id'] . "'><input type='button' name='delete' value='Delete' class='button'></a></div><br><div>Item Name: " . $row['item_name'] . "</div><br><div>Description: " . $row['item_description'] . "</div><br><div>Cost: $" . $row['item_cost']  . "</div><br><div><img src='images/" . $row['item_image'] . "' class='item_img'></div><br></section>";
		}
	?>
</div>
</div>
</body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146891297-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
 	 function gtag(){dataLayer.push(arguments);}
 	 gtag('js', new Date());

 	 gtag('config', 'UA-146891297-1');
	</script>
</html>