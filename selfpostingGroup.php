<?php

include "validationClass/FormValidation.php";
$validateTool = new FormValidation(); //instantiate new object

$inCourseNumber = "";
$radioS = "";

$test6 = "spring";
$test7 = "summer";
$test8 = "fall";

$isValid1 = true;
$isValid2 = true;
$errorMessage1 = "";
$errorMessage2 = "";
$successMessage = "";


if (isset($_POST['submit']) ) {
  if (isset($_POST['radioS'])) {
    $radioS = $_POST['radioS'];
  }
  $inCourseNumber = $_POST['course'];
    if ($validateTool->validateRequiredNumeric($inCourseNumber) && strlen($inCourseNumber) == 3) {
      $isValid1 = true;
      }

      else {
        $isValid1 = false;
        $errorMessage1 = "This field is Invalid";
        $successMessage = "";
      }
      if ($validateTool->validateRequiredField($radioS)) {
        $isValid2 = true;
        }

        else {
          $isValid2 = false;
          $errorMessage2 = "This field is Invalid";
          $successMessage = "";
        }
      if ($isValid1 && $isValid2) {
        $errorMessage1 = "";
        $errorMessage2 = "";
        $successMessage = "Thanks for your submission";
        $inCourseNumber = '';
        }
}

 ?>

<!DOCTYPE html>
<html>
<head>
  <title>Form Submitted</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
body {
  margin: 0 auto;
  padding: 0;
  width: 100vw;
  height: 100vh;
}
.containerm {
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  justify-content: center;
  margin:0;
  padding: 0;
}
.wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  justify-content: center;
  margin:0;
  padding: 0;
}
form {
  width: 300px;
  height: 400px;
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  margin:5px;
  padding: 50px;
  border: 2px solid black;
}
h1, label, p {
  font-family: Arial, Helvetica, sans-serif;
}

.error {
  color: red;
  margin: 0;
  padding: 0;
}

.success {
  color: green;
  margin: 0;
  padding: 0;
}

  </style>
</head>

<body>
  <div class="containerm">
    <div class="wrapper">
      <h1>Contact Us</h1>
      <p class="success"><?php echo $successMessage ?></p>
      <form class="" action="selfpostingGroup.php" method="post">
        <label for="name">Course Number: </label>
        <input type="text" name="course" value="<?php echo $inCourseNumber ?>" ><p class="error"><?php echo $errorMessage1 ?></p>
        <p>Semester:</p>
        <p class="error"><?php echo $errorMessage2 ?></p>
        <label>Spring
        <input type="radio" name="radioS[]" value="spring" <?php if (isset($_POST['submit']) && is_array($radioS) && in_array($test6, $radioS) == "1") { echo 'checked';  } else {echo '';} ?>></label>
        <label>Summer
        <input type="radio" name="radioS[]" value="summer" <?php if (isset($_POST['submit']) && is_array($radioS) && in_array($test7, $radioS) == "1") { echo 'checked'; } else {echo '';} ?>></label>
        <label>Fall
        <input type="radio" name="radioS[]" value="fall" <?php if (isset($_POST['submit']) && is_array($radioS) && in_array($test8, $radioS) == "1") { echo 'checked'; } else {echo '';} ?>></label>
        <input type="submit" name="submit" value="Submit">
        <input type="reset" name="reset">
      </form>
    </div>
  </div>
</body>
</html>
